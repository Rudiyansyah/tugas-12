@extends('layout.master')
@section('judul')
    
        Halaman Tambah
        @endsection
        @section('content')

        <form action="/cast" method="post">
        @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="nama" class="form-control">
  </div>
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" class="form-control">
  </div>
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" cols="30" class="form-control"></textarea>
    
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
    
