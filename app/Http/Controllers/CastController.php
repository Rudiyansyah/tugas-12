<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function create()
    {
        return view('post.create');
    }

    public function index()
    {
        //$cast = DB::table('cast')->all();
 
        //return view('post.index', compact('cast'));
        $cast2 = DB::table('cast')->get();
 //dd($cast2);
        return view('post.index', compact('cast2'));
        //return view('post.index');

    }

    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'nama' => 'required|max:25',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert(
    [
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio']
    ]
    
);
return redirect('/cast');
    }
    
    public function show($id)
    {

    $cast2 = DB::table('cast')->where('id', $id)->first();//ambil 1 data saja
    return view('post.detail',compact('cast2'));
    }

    public function edit($id)
    {

    $cast2 = DB::table('cast')->where('id', $id)->first();//ambil 1 data saja
    return view('post.edit',compact('cast2'));
    }


    public function update(Request $request,$id)
    {
        //dd($request->all());
        $request->validate([
            'nama' => 'required|max:25',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        

    DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
            );
           return redirect('/cast'); 
    }

    public function destroy(Request $request,$id)
    {
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast'); 
    }
    
}
