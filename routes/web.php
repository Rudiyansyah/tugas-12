<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello-laravel', function () {
    return "hallo laravel 6";
});*/

Route::get('/', 'HomeController@Index');
Route::get('/register', 'AuthController@Register');
Route::post('/welcom', 'AuthController@Welcom');

//Route::get('/master', function(){
//    return view('layout.master');
//});

Route::get('/table', function(){
    return view('halaman.table');
});

Route::get('/data-table', function(){
    return view('halaman.datatable');
});


Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast', 'CastController@index');
Route::get('/cast/{id}', 'CastController@show');
Route::get('/cast/{id}/edit', 'CastController@edit');
Route::put('/cast/{id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');
/*


*/
